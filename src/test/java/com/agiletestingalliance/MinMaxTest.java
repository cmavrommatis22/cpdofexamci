package com.agiletestingalliance;
import org.junit.Test;
import static org.junit.Assert.*;



public class MinMaxTest {
	@Test
	public void testGreaterNumb() throws Exception {
		final int greaterParam = new MinMax().greaterNumb(10,8);
		assertEquals("Greater number",10,greaterParam);
		final int secondGrtr = new MinMax().greaterNumb(2,8);
		assertEquals("Greater number",8,secondGrtr);
	}
	@Test
	public void testBar() throws Exception {
		String testStr = new MinMax().bar("Hello-Devops");
		assertTrue("Check string",testStr.contains("Devops"));

		String testStr2 = new MinMax().bar("");
		assertEquals("Check string","",testStr2);


	}

}
