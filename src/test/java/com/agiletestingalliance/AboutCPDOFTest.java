package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;


public class AboutCPDOFTest{

	@Test
	public void testDesc() throws Exception {
		String testStr = new AboutCPDOF().desc();
		System.out.print(testStr);
		assertTrue("AboutCPDOF",testStr.contains("CP-DOF certification program"));

	}
}	
